import { ref } from "vue";

var Web3 = require('web3')
var web3 = new Web3("ws://localhost:8545")
let transactionHash = ref("")
let transactionDatas = ref({})

export default function useTransaction() {
    const makeNewTransaction = async(transaction) => {
        await web3.eth.sendTransaction(transaction)
            .on('transactionHash', (hash) => {
                transactionHash.value = hash;
                getTransactionDatas(transactionHash.value);
            });
    }

    const getTransactionDatas = async(hash) => {
        transactionDatas.value = await web3.eth.getTransaction(hash);
        console.log(transactionDatas.value);
    }
    return {
        makeNewTransaction,
        transactionDatas
    }
}