import { ref, onMounted } from 'vue'

var Web3 = require('web3')
var web3 = new Web3("ws://localhost:8545")

export default function useAccountsDatas() {
    const accounts = ref([])

    const getBalance = async(account) => {
        return await web3.eth.getBalance(account).then(balance => {
            return balance;
        })
    }

    const getAccounts = async() => {
        accounts.value = []
        await web3.eth.getAccounts().then(res => {
            res.forEach(async(account) => {
                const balance = await getBalance(account);
                const data = {
                    account: account,
                    balance: balance
                }
                accounts.value.push(data)
            });
        })
    }
    onMounted(() => {
        getAccounts();
    })
    return {
        accounts,
        getAccounts
    }
}