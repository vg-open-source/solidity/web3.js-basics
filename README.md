# ui

Apply basics from Web3.js library. Communication with an ethereum node.
Inspired by [this blog post](https://blog.openzeppelin.com/a-gentle-introduction-to-ethereum-programming-part-1-783cc7796094/).
Front made with Vue.js 3.

## Project setup

Make sure you have testrpc launched. See [here](https://github.com/trufflesuite/ganache-cli).

```
npx ganache-cli
npm install
npm run serve
launch localhost:8080 to see the result.
```